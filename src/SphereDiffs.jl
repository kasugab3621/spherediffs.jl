"""
Tools of discrete differentials for spherical data (longitude-latitude mesh data).
"""
module SphereDiffs
using EllipsisNotation
export dx, dy, dz


function zonal_loop(lons::Vector)
    dx = lons[2] - lons[1]
    lons_end_dx = lons[end] + dx
    if lons_end_dx == lons[1] || lons_end_dx - 360. == lons[1] || lons_end_dx + 360. == lons[1]
        return true
    else
        return false
    end
end


"""
make numpy.meshgrid-like arrays with Fortran order
"""
function mk_meshgrid(lons::Vector, lats::Vector)::Tuple{Array, Array}
    # meshgrid
    lons_mesh = lons .* ones(length(lats))'
    lats_mesh = ones(length(lons)) .* lats'
    return lons_mesh, lats_mesh
end


"""
Eastward differential
For global data, central difference in all grids.
For regional data, central difference in the inner grids and foward/backward difference for the first/last boundary.
"""
function dx(ar::Array, lons::Vector, lats::Vector; R=6367.5*1e3, pole=0.)

    dlon = lons[2] - lons[1]
    s = size(ar)
    s1 = Base.setindex(s, s[1]+2, 1)  # wide array shape
    ar2 = Array{eltype(ar)}(undef, s1)  # empty wide array
    lons2 = Vector{eltype(lons)}(undef, s1[1])

    # add cyclic back and forth
    if zonal_loop(lons)
        #ar2 = vcat(vcat(ar[end,:,..]', ar), ar[1,:,..]')
        #lons2 = push!(pushfirst!(lons, lons[1]-dlon), lons[end]+dlon)
        ar2[1,..] = ar[end,..]
        ar2[2:end-1,..] = ar
        ar2[end,..] = ar[1,..]
        lons2[1] = lons[end]
        lons2[2:end-1] = lons
        lons2[end] = lons[1]
    else
        #ar2 = vcat(vcat(ar[1,:,..]', ar), ar[end,:,..]')
        #lons2 = push!(pushfirst!(lons, lons[1]), lons[end])
        ar2[1,..] = ar[1,..]
        ar2[2:end-1,..] = ar
        ar2[end,..] = ar[end,..]
        lons2[1] = lons[1]
        lons2[2:end-1] = lons
        lons2[end] = lons[end]
    end

    # mk center difference of inpout
    ar_center_diff = ar2[1+2:end,..] - ar2[1:end-2,..]

    # mk dx
    _lon = lons2[1+2:end] - lons2[1:end-2]
    dlons_mesh, lats_mesh = mk_meshgrid(_lon, lats)
    dx_center_diff = @. 2. * pi * R * cos(deg2rad(lats_mesh)) * dlons_mesh / 360.
    
    # mk eastward differential
    east_diff = ar_center_diff ./ dx_center_diff 
    return @. ifelse(east_diff == Inf, pole, east_diff)  # replace values at poles

end


"""
Meridional differential
For both global and regional data, central difference in the inner grids and foward/backward difference for the first/last boundary.
"""
function dy(ar::Array, lons::Vector, lats::Vector; R=6367.5*1e3, pole=0.)

    s = size(ar)
    s1 = Base.setindex(s, s[2]+2, 2)  # wide array shape
    ar2 = Array{eltype(ar)}(undef, s1)  # empty wide array
    lats2 = Vector{eltype(lats)}(undef, s1[2])

    # add cyclic back and forth
    ar2[:,1,..] = ar[:,1,..]
    ar2[:,2:end-1,..] = ar
    ar2[:,end,..] = ar[:,end,..]
    lats2[1] = lats[1]
    lats2[2:end-1] = lats
    lats2[end] = lats[end]

    # mk center difference of inpout
    ar_center_diff = ar2[:,1+2:end,..] - ar2[:,1:end-2,..]

    # mk dx
    _lat = lats2[1+2:end] - lats2[1:end-2]
    lons_mesh, dlats_mesh = mk_meshgrid(lons, _lat)
    dy_center_diff = @. 2. * pi * R * dlats_mesh / 360.  # meter

    # mk eastward differential
    meri_diff = ar_center_diff ./ dy_center_diff 
    return @. ifelse(meri_diff == Inf, pole, meri_diff)  # replace values at poles

end


"""
Vertical differential for 3D or higher dimensional data
Any vertical units are available for vertical coodinate, but if units="hPa", it will be converted to Pa.
"""
function dz(ar::Array, levs::Vector; R=6367.5*1e3, units="Pa", z_dims=3)

    s=size(ar)

    NZ=s[z_dims]
    dlev = fill(NaN,NZ)

    dlev[1] = levs[2]-levs[1]
    #dlev[2:end-1] = (levs[3:end] .- levs[1:end-2]) ./ 2
    dlev[2:end-1] = (levs[3:end] .- levs[1:end-2])
    dlev[end] = levs[end]-levs[end-1]
    dlev1 = reshape(dlev, 1,1,NZ)  # for broadcast

    if units == "hPa"  # convert hPa -> Pa
        dlev2 = dlev1 .* 100
    else  # if Pa, returns (unit)/Pa; if m, returns (unit)/m
        dlev2 = dlev1
    end

    dardz = fill(NaN,s)
    dardz[:,:,1,..] = (ar[:,:,2,..] .- ar[:,:,1,..]) ./ dlev2[:,:,1]
    #dardz[:,:,2:end-1,..] = (ar[:,:,3:end,..] .- ar[:,:,1:end-2,..]) ./ dlev2[:,:,2:end-1] ./ 2
    dardz[:,:,2:end-1,..] = (ar[:,:,3:end,..] .- ar[:,:,1:end-2,..]) ./ dlev2[:,:,2:end-1]
    dardz[:,:,end,..] = (ar[:,:,end,..] .- ar[:,:,end-1,..]) ./ dlev2[:,:,end]

    return dardz

end

end
