using SphereDiffs
using Test

@testset "SphereDiffs.jl" begin
    # Write your tests here.
    ar = convert(Array{Float64}, collect(reshape(1:64, 4,4,4)))
    lons = [1., 2., 3., 4.]
    lats = [1., 2., 3., 4.]
    levs = [1000, 900, 800, 700]
    result_dx = dx(ar, lons, lats)
    result_dy = dy(ar, lons, lats)
    result_dz = dz(ar, levs, units="hPa")
end
