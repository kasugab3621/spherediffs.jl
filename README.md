# SphereDiffs.jl
- A simple (simplest?) julia package to caluculate zonal/meridional/vertical differentials for an Array with longitude-latitude mesh.

- The longitude mesh must be evenly spaced. The latitude mesh can be either evenly spaced and not enenly spaced (experimental).

- Higher dimensinal Arrays (3D, 4D, ..) can be used, thanks to [EllipsisNotation.jl](https://docs.sciml.ai/EllipsisNotation/stable/). But not compatible to 1D (1D/2D) arrays for dx/dy (dz), for now...

<!--
- Zonal differential: Central difference for global data and for inner mesh of regional data. Forward and Backward difference for the first and last boundaries.

- Meridional differential: Central difference for inner mesh of global and regional data. Forward and Backward difference for the first and last boundaries.
-->

## Installation

```julia: julia
using Pkg
Pkg.add(url="https://gitlab.com/kasugab3621/spherediffs.jl")
```

## How to use
```julia: julia
using SphereDiffs

# zonal differential
dudx = dx(u, lons, lats)

# meridional differential
dudy = dy(u, lons, lats)

# vertical differential
dudz = dz(u, levs, units="hPa")
```
<!--You can change earth radius by set `dx(u, lons, lats, R=6378.1*1e3)` in meter. Its default is `R=6367.5*1e3`, which is a mean value of the equitorial Earth radius and the polar Earth radius.-->

You can change values on the North/South Pole by adding an option `pole=undef`. Its default is `0.`  

For `dz`, you do not have to set `units`, and units of the return value obey units of `levs`. If add `units="hPa"`, units of `levs` are automatically converted to Pa.
